﻿//Make sure to place this script in a folder called "Editor"
using UnityEditor;
using UnityEngine;
using System.Collections;

public class MyTools
{


	[MenuItem("MyTools/CreateGameObjects")]
	static void Create()
	{
		GameObject[] boxes = new GameObject[200];
		Vector3 left_box_location = GameObject.Find("CubePos").transform.position;
		for (int i = 0; i < boxes.Length; i++) {
			boxes[i] = GameObject.CreatePrimitive(PrimitiveType.Cube); //(GameObject)Instantiate(Resources.Load("Prefabs/Cube"));
			boxes[i].transform.position = left_box_location;
			boxes[i].transform.Translate( (i*2f), 0f, 0f);
		}
	}
}