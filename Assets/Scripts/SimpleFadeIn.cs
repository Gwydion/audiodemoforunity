﻿using UnityEngine;
using System.Collections;

//Fades out audio and SelfDestructs
public class SimpleFadeIn : MonoBehaviour {
	public float targetVolume = 1f;
	public float changeAmountPerUpdate = 0.02f;

	void Start() {
		audio.velocityUpdateMode = AudioVelocityUpdateMode.Fixed;
		SimpleFadeOut toBeDeleted = gameObject.GetComponent<SimpleFadeOut>();
		if (toBeDeleted != null)
			Destroy(toBeDeleted);
	}

	void FixedUpdate () {
		audio.volume += changeAmountPerUpdate;
		if (audio.volume >= targetVolume) {
			audio.volume = targetVolume;
			Destroy(this);
		}
	}

}
