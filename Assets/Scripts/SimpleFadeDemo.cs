﻿using UnityEngine;
using System.Collections;

[RequireComponent( typeof(AudioSource) )]
public class SimpleFadeDemo : MonoBehaviour {
	public bool isOn = false;
	public string fadeKey = "1";
	public float maxVol = 1f;


	// Update is called once per frame
	void Start() {
		if (audio.volume > 0)
			isOn = true;
	}

	void Update () {
		if (Input.GetKeyDown(fadeKey))
			turnOnOff();
	}

	/*-------------------------------------------------*/
	/*Trigger functions for simple fade in and fade out */
	/*-------------------------------------------------*/

	public void fadeIn() {
		gameObject.AddComponent<SimpleFadeIn>().targetVolume = maxVol;
		isOn = true;
	}

	public void fadeOut() {
		gameObject.AddComponent<SimpleFadeOut>();
		isOn = false;
	}

	public void turnOnOff() {
		if (isOn)
			gameObject.AddComponent<SimpleFadeOut>();
		else gameObject.AddComponent<SimpleFadeIn>().targetVolume = maxVol;;
		isOn = !isOn;
	}
	
/*-----------------------------------------------*
  * Trigger function for fade with animation curves *
   *--------------------------------------------------*/ 
	/*
	//onOff switch for animation fade
	public void fadeWithAnimation() {
		GameObject go = (GameObject)Instantiate(Resources.Load("Prefabs/FadeVolume"));
		go.transform.parent = transform;
		isOn = !isOn;
		go.GetComponent<AnimationFade>().init(isOn);
	}

	public void fadeInAnimation(int inOrOut = 0) {
		isOn = true;
		GameObject go = (GameObject)Instantiate(Resources.Load("Prefabs/FadeVolume"));
		go.transform.parent = transform;
		isOn = !isOn;
		go.GetComponent<AnimationFade>().init(isOn);
	}

	public void fadeOutAnimation() {
		isOn = true;
		GameObject go = (GameObject)Instantiate(Resources.Load("Prefabs/FadeVolume"));
		go.transform.parent = transform;
		go.GetComponent<AnimationFade>().init(isOn);
	}
	*/
}
