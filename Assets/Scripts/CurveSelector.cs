﻿using UnityEngine;
using System.Collections;

public class CurveSelector : MonoBehaviour {
	public Transform playerSpwanPoint;
	public Transform objectSpawnPoint;
	public string firstToLoad = "WaterFallsSpot";
	public GameObject currentTestObject;

	private string pathToPrefabs = "Prefabs/";

	// Use this for initialization
	void Start () {
		RenderSettings.ambientLight = Color.black;
		currentTestObject = (GameObject) Instantiate( Resources.Load(pathToPrefabs + firstToLoad) );

		currentTestObject.transform.position = objectSpawnPoint.transform.position;
		currentTestObject.transform.rotation = objectSpawnPoint.transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("1"))
			startTest("WaterFallsSpot");
		if (Input.GetKeyDown("2"))
			startTest("CampFireSpot");
		if (Input.GetKeyDown("3"))
			startTest("MusicCubeSpot");
	}

	void startTest(string prefabName)  {
		transform.position = playerSpwanPoint.position;
		transform.rotation = playerSpwanPoint.rotation;

		Destroy(currentTestObject);

		currentTestObject = (GameObject) Instantiate( Resources.Load(pathToPrefabs + prefabName) );
		currentTestObject.transform.position = objectSpawnPoint.transform.position;
		currentTestObject.transform.rotation = objectSpawnPoint.transform.rotation;

	}
}
