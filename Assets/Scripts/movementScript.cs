﻿ using UnityEngine;
using System.Collections;

/*
 * This is a really simple 3D moving script for character controller.
 */ 
public class movementScript : MonoBehaviour {
	public float speed = 10.0F;
	public float jumpSpeed = 8.0F;
	public float gravity = 20.0F;
	private Vector3 moveDirection = Vector3.zero;
	public Transform camera;

	void Start() {
		Screen.showCursor = false;
		Screen.lockCursor = true;
	}

	void Update() {
		CharacterController controller = GetComponent<CharacterController>();
		if (controller.isGrounded) {
			moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
			moveDirection = camera.TransformDirection(moveDirection);
			moveDirection *= speed;
			if (Input.GetButton("Jump"))
				moveDirection.y = jumpSpeed;
		}
		moveDirection.y -= gravity * Time.deltaTime;
		controller.Move(moveDirection * Time.deltaTime);

		transform.Rotate(Vector3.up * Time.deltaTime * 100f * Input.GetAxis("Mouse X"));
		camera.Rotate(Vector3.left * Time.deltaTime * 100f * Input.GetAxis("Mouse Y"));
	}

}