﻿using UnityEngine;
using System.Collections;

public class SetLoopWithKey : MonoBehaviour {
	GaplessLooper gaplessLooper;
	// Use this for initialization
	void Start () {
		gaplessLooper = gameObject.GetComponent<GaplessLooper>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.anyKeyDown) {
			if (Input.GetKeyDown("1"))
				gaplessLooper.changeNextClipNow(0);
			else if (Input.GetKeyDown("2"))
				gaplessLooper.changeNextClipNow(1);
			else if (Input.GetKeyDown("3"))
				gaplessLooper.changeNextClipNow(2);
		}
	}
}
