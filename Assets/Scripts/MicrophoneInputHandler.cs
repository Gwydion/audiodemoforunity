﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (AudioSource))]
public class MicrophoneInputHandler : MonoBehaviour {

	//Length of recorded clip, min 1 second
	private int cliplength = 1;

	private float recordingStartTime;
	private int recordingsWaiting = 0;
	public string recordKey = "x";
	public GameObject ThreeDAudioSource;
	private AudioClip ThreeDClip;

	// Use this for initialization
	void Start () {
		string[] mic_devices = Microphone.devices;

		//Print microphone device names
		foreach (string name in mic_devices) {
			Debug.Log(name);
		}

	}

	//Check input
	void Update() {
		if (Input.GetKeyDown(recordKey)) {
			recordingsWaiting++;
			StartCoroutine("micButtonDown");
		}

		if (Input.GetKeyUp(recordKey)) {
			Invoke("micButtonUp", 1f );
		}

		if (Input.GetKeyDown("1")) {
			record3DSound();
		}

		if (Input.GetKeyUp("1")) {
			stopRecording3DSound();
		}
	}

	//If mic is not already recordin, starts recordin and plays AudioSource
	IEnumerator micButtonDown() {
		if ( Microphone.IsRecording(null) == false ) { // null here instead of device name gives the default microphone
			audio.clip = Microphone.Start(null, true, cliplength, 44100);
			yield return new WaitForSeconds( (0.1f) );
			audio.loop = true;
			audio.Play();
		}
	}

	//If mic is recordin, recording is ended and AudioSource is stopped
	void micButtonUp() {
		recordingsWaiting--;
		if (recordingsWaiting == 0) {
			Microphone.End(null);
			audio.Stop ();
		}
	}

	void record3DSound() {
		if ( Microphone.IsRecording(null) == false ) { // null here instead of device name gives the default microphone
			ThreeDClip = Microphone.Start(null, true, 3, 44100);
		}
	}

	void stopRecording3DSound() {
		Microphone.End(null);
		float[] data = new float[ThreeDClip.samples];
		ThreeDClip.GetData(data, 0);

		ThreeDAudioSource.audio.clip = AudioClip.Create("juttu",  (ThreeDClip.samples), 1, 44100, true, false);
		ThreeDAudioSource.audio.clip.SetData(data, 0);

		ThreeDAudioSource.audio.loop = true;
		ThreeDAudioSource.audio.spread = 180f;
		ThreeDAudioSource.audio.Play();
	}

}
