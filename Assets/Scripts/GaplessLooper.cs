﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (AudioSource))]
public class GaplessLooper : MonoBehaviour {

	public AudioClip ringClip;
	public GameObject LoopLayer;

	//length of the 
	private int ringClipLength;
	private int sampleRate = 44100;
	public float ringClipSeconds = 0f; //Must be at least the length of longest loop

	//sample position where next data-chunk (audio clip) is   
	public int next_data_point;
	public int last_data_point;
	//clips to be played
	public AudioClip[] loops;

	public int nextClipToPlay = 0;

	//MONITORING PARAMETERS
	public int changeLimit = 5000;
	private int difference; //calculated difference of next_data_point and playback position (in samples)
	private int distance; //how many samples away the next_data_point is from playback position (in samples)
	public int SampleLimit = 44100; //when distance is less than SampleLimit same clip is added again
	
	void Awake () {
		//set ring-clips length in samples
		ringClipLength = (int) ( sampleRate * ringClipSeconds );

		//create playback loop -clip 
		ringClip = AudioClip.Create("RingClip", ringClipLength ,2,44100, false, false);
		audio.clip = ringClip;

		//set first 2 data chunks (clips) to ringClip
		attachNextClip(0);
		//attachNextClip(0);

		audio.loop = true;
		//Invoke("scheduleStart", 2);
		audio.Play();

		StartCoroutine("moniotrClip");
	}
	

	//attach new chunk of data (audio clip) to the ringClip
	void attachNextClip(int nro, bool toLastDataPoint=false) {

		if (toLastDataPoint) {
			next_data_point = last_data_point;
		}	

		Debug.Log("DataPoint " + next_data_point);
		//get audio data for the new clip
		float[] audioData = GetDataForBeatNb(nro);

		//calculate the end point of the new clip
		int end_of_new_clip = ( next_data_point + loops[nro].samples );

		//Case 1: new clip "fits" in the ring
		if ( end_of_new_clip < ringClipLength) {
			ringClip.SetData(GetDataForBeatNb(nro), next_data_point);
			last_data_point = next_data_point;
			next_data_point += loops[nro].samples;
		
		} else {
			//Case 2: new clip "fills" the ring perfectly
			if((end_of_new_clip == ringClipLength)) {

				ringClip.SetData(GetDataForBeatNb(nro), next_data_point);

				last_data_point = next_data_point;
				next_data_point = 0;

			//Case 3: new clip "doesn't fit" the ring
			//so it's split in half (first half to the end, second half to the beginning of the ring)
			} else {

				//calculate index where the audio data is splitted
				int split_index = (ringClipLength - next_data_point)*2;

				//arrays for the splitted audio data
				float[] data_to_start = new float[(( audioData.Length - split_index ))];
				float[] data_to_end  = new float[split_index];

				//the split
				System.Array.Copy(audioData, 0, data_to_end, 0, (data_to_end.Length));
				System.Array.Copy(audioData, split_index, data_to_start, 0, ( (data_to_start.Length) ) );

				//setting clips to end and beginning of the ring
				ringClip.SetData(data_to_end, next_data_point);
				ringClip.SetData(data_to_start, 0);

				last_data_point = next_data_point;
				next_data_point = (data_to_start.Length/2);

				//Debug prints
				Debug.Log("---------SLPLIT----------");
				Debug.Log("RCL: " + ringClipLength);
				Debug.Log("NextDataSet: " + next_data_point);
				Debug.Log("AudioDataLength: " + audioData.Length);
				Debug.Log("SplitIndex: " + split_index);
				Debug.Log("start length: " + data_to_start.Length);
				Debug.Log("end length: " + data_to_end.Length);
				Debug.Log("CLip 0 samples " + loops[0].samples);
				Debug.Log("CLip 1 samples " + loops[1].samples);
				Debug.Log ("datatoend " + data_to_end.Length);
				Debug.Log ("datatoSTart  " + data_to_start.Length);

			}
		}
	}

	//returns folat[] data of loop number loopNb
	float[] GetDataForBeatNb(int loopNb) {			
		float[] samples = new float[(loops[loopNb].samples * loops[loopNb].channels)];
		loops[loopNb].GetData(samples, 0);
		return samples;	
	}

	//monitors palyback posiion
	IEnumerator moniotrClip() {
		while (true) {
			difference = ( next_data_point - audio.timeSamples );
			distance = 0;

			if (difference < 0) {
				distance = ( ringClipLength - ( audio.timeSamples - next_data_point) );
			}else distance = difference;
			if (distance < SampleLimit) attachNextClip(nextClipToPlay);

			yield return new WaitForSeconds(0.03f);
		}
	}

	//changes currently playing clip
	public void changeNextClipNow(int next_clip) {
		nextClipToPlay = next_clip;
		if ( distance > changeLimit ) {
			attachNextClip(nextClipToPlay, true);
		}
	}

	//changes next clip to play after currently playing one
	public void changeNextClip(int next_clip) {
		nextClipToPlay = next_clip;
	}

	void Update() {
		if (Input.GetKeyDown("a")) {
			loops[2] = GameObject.Find("drumLoop03").audio.clip;
		}
	}
}
