﻿using UnityEngine;
using System.Collections;


[RequireComponent (typeof (AudioSource))]
public class VizualizeAudioSpectrum : MonoBehaviour {
	private float[] spectrum = new float[4096];

	public GameObject[] boxes = new GameObject[300];
	public Transform left_box_location;
	
	// Use this for initialization
	void Start () {
		//boxes =  GameObject.FindGameObjectsWithTag("musicbox");
		for (int i = 0; i < boxes.Length; i++) {
			boxes[i] = (GameObject)Instantiate(Resources.Load("Prefabs/Cube"));
			boxes[i].transform.position = left_box_location.transform.position;
			boxes[i].transform.Translate( (i*2f), 0f, 0f);
		}

		RenderSettings.ambientLight = Color.black;
		
	}
	
	// Update is called once per frame
	void Update () {
		audio.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);
		for (int i = 0; i < boxes.Length; i++) {
			boxes[i].transform.localScale = new Vector3(1f, (1f + spectrum[i]*2000f), 1f);
		}
	}
}
