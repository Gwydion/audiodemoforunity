﻿using UnityEngine;
using System.Collections;

//This is ONLY A DEMO of how SFX and SFXPlayer scripts can be accessed from other scripts
//This is not a preferred way to trigger SFX
public class SFXDemoOfUse : MonoBehaviour {
	SFXPlayer sound;
	CharacterController controller;
	bool playWalking = false;

	// Use this for initialization
	void Start () {
		sound = GetComponent<SFXPlayer>();
		controller = GetComponent<CharacterController>();
		StartCoroutine("playWalkingSound");
	}
	
	// Update is called once per frame
	void Update () {
		if ( ( Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0 ) && controller.isGrounded ) {
				playWalking = true;
		} else playWalking = false;

		if (Input.GetMouseButtonDown(0)) {
			sound.playSFX("pim");
		}
	}

	//Coroutine checks every 0.2 secs if walking sound is to be played
	//NOT PREFERRED WAY to trigger SFX.
	IEnumerator playWalkingSound() {
		while (true) {
			if (playWalking) {
				sound.playSFX("steps");
			}
			yield return new WaitForSeconds(0.2f);
		}

	}
}
