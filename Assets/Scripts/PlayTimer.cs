﻿using UnityEngine;
using System.Collections;

[RequireComponent( typeof(AudioSource) )]
public class PlayTimer : MonoBehaviour {
	public double startTime;

	// Use this for initialization
	void Start () {
		startTime = AudioSettings.dspTime;
	}
	
	public void playAfter(PlayTimer nextToPlay) {
		if (audio.isPlaying == false) return; //works only when audio is playing
		if (startTime > AudioSettings.dspTime) return; //works only when audio really playing, not just scheduled

		Debug.Log(gameObject.name);

		double nextStartTime = startTime + ( (double) audio.clip.samples / 44100.0);
		nextToPlay.startTime = nextStartTime;
		nextToPlay.audio.PlayScheduled(nextStartTime);
	}

	//start time needs to be set
	//when playing audio for playAfter to work
	public void play() {
		audio.Play();
		startTime = AudioSettings.dspTime;
	}
}
