﻿using UnityEngine;
using System.Collections;

public class AnimationFade : MonoBehaviour {
	bool directionUp = true;
	AudioSource pAudio;
	Animation ani;
	string animation_to_play = "fadeSlow";
	// Use this for initialization
	
	// Update is called once per frame
	void Update () {
		//Witch way the curve will be applied
		if (directionUp) pAudio.volume = transform.position.x;
		else pAudio.volume = ( 1 - transform.position.x );

		//Self destruct when animation ends
		if (ani.isPlaying == false)
			Destroy(gameObject);
	}

	//init function is used instead of Awake() to get access to new parent
	public void init(bool directionUp, string fadeCurve = "fadeSlow") {
		this.directionUp = directionUp;
		pAudio = transform.parent.audio;
		ani = GetComponent<Animation>();
		ani.Play();
		animation_to_play = fadeCurve;
		Transform tra = transform.parent.Find("AnimationFade");
		if (tra != null) {
			Destroy(tra);
		}
	}
}
