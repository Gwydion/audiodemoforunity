﻿using UnityEngine;
using System.Collections;

//Esimerkki käytöstä..
public class SFXPlayer : MonoBehaviour {
	public SFX[] SoundEffects;

	// Use this for initialization
	void Start () {
		SoundEffects = GetComponents<SFX>();
	}

	 public void playSFX(string effectName) {
		foreach (SFX sfx in SoundEffects) {
			if (sfx.collectionName == effectName) {
				sfx.PlayClip();
				break;
			}
		}
	}
}
