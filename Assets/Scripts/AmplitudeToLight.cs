using UnityEngine;
using System.Collections;

public class AmplitudeToLight : MonoBehaviour {
	private float[] samples = new float[128];
	// Use this for initialization
	void Start () {
		RenderSettings.ambientLight = Color.black;
	}
	
	// Update is called once per frame
	void Update () {
		convertAmpitudeToLight();
	}


	void convertAmpitudeToLight() {
		//channel 0 is left, channel 1 is right
		audio.GetOutputData(samples, 0);

		//calculate RMS
		float sum = 0f;
		float avrg = 0f;
		   

		foreach (float f in samples) {
			sum += Mathf.Pow(f, 2);
		}

		avrg = Mathf.Sqrt( (sum/samples.Length) );
		Debug.Log (avrg);
		//direct arvrg value to light source intensity
		light.intensity = avrg*10.0f;
	}
}
