﻿using UnityEngine;
using System.Collections;

//Fades in audio and SelfDestructs
public class SimpleFadeOut : MonoBehaviour { 
	// Update is called once per frame
	float changeAmountPerUpdate = 0.02f;
	public float targetVolume = 1f;

	void Start() {
		audio.velocityUpdateMode = AudioVelocityUpdateMode.Fixed;

		SimpleFadeIn toBeDeleted = gameObject.GetComponent<SimpleFadeIn>();
		if (toBeDeleted != null)
			Destroy(toBeDeleted);
	}

	void FixedUpdate () {
		audio.volume -= changeAmountPerUpdate;
		if (audio.volume < changeAmountPerUpdate) {
			audio.volume = 0f;
			Destroy(this);
		}
	}

}
