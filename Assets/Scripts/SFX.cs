﻿using UnityEngine;
using System.Collections;


//Automatically adds AudioSource and SFXPlayer to Gameobject if needed
[RequireComponent (typeof (AudioSource))]
[RequireComponent (typeof (SFXPlayer))]

public class SFX : MonoBehaviour {
	public string collectionName; //the name of the folder where audio files are
	private string path_to_collections = "Audio/SFX/Collections/"; //path to the folder where all collection folders are
	private AudioClip[] clips; //clips (different versions of same sound effect). This will be filled automaically.
	private int lastPlayed; //index nuber of last played clip

	// Use this for initialization
	void Start () {
		//Get all clips from folder with name set in collectionName
		Object[] clips_obj = Resources.LoadAll(path_to_collections + collectionName);

		//Fill the "clips" array
		if (clips_obj.Length > 0 ) {
			clips = new AudioClip[clips_obj.Length];

			for (int i = 0; i < clips_obj.Length; i++) {
				clips[i] = (AudioClip) clips_obj[i];
			}
		} else {
			//print error if collection could not be found or folder is empty
			Debug.LogError( "Could not locate collection " + name );
		}

	}

	//Returns next random clip index (not same as previously played)
	public int getNextClipIndex() {
		int nextClipIndex = Random.Range (0, clips.Length);
		if (nextClipIndex == lastPlayed)
			//if same as next played will play next clip. If last will play first.
			nextClipIndex = ( (nextClipIndex+1)%clips.Length );
		return nextClipIndex;
	}

	//Plays a random clip (not same as previously played)
	public void PlayClip() {
		int clip_to_play_next = getNextClipIndex();
		audio.PlayOneShot( clips[ clip_to_play_next ] );
		lastPlayed = clip_to_play_next;
	}

}
