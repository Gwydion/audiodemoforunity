﻿using UnityEngine;
using System.Collections;

//To make sure all music AudioSources start playing
//exactly same time
public class PlayscheduleStart : MonoBehaviour {

	// Use this for initialization
	void Start () {
		audio.PlayScheduled(0.1);
	}
}
