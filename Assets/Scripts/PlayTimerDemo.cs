﻿using UnityEngine;
using System.Collections;

//Simple control script to demonstrate PlayTimer scripts
public class PlayTimerDemo : MonoBehaviour {
	int nextToPlay = 0;
	public PlayTimer[] demoTimers = new PlayTimer[2]; //playTimers for demo
	public bool noneIsPlaying = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		//check what key was pressed
		if (Input.anyKeyDown) {
			int keyNuber;
			if (Input.GetKeyDown("1"))
				keyNuber = 0;
			else if (Input.GetKeyDown("2")) 
				keyNuber = 1;
			else return;

			if (noneIsPlaying) //if none is playing can't use playAfter
				demoTimers[keyNuber].play(); //NOT audio.play.. sets also startTime
			else demoTimers[nextToPlay].playAfter(demoTimers[keyNuber]);

			nextToPlay = keyNuber;
		}


		if ( demoTimers[ nextToPlay ].audio.isPlaying == false)
			//if none is playing at the time
			noneIsPlaying = true;
		else noneIsPlaying = false;
	}
}
